package com.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class TASK3Test {

	@Test
	public void test() {
		TASK3 task3 = new TASK3();
		List<String> list = new ArrayList<String>();
		list = Arrays.asList("Write", "a", "list", "and", "add", "an", "aleatory", "number", "of", "Strings", "In", "the", "end", "print", "out", "how", "many", "distinct", "itens", "exists", "on", "the", "list");
		Assert.assertNotEquals(list.size(), task3.countDistinctItems(list));
	}

}
