package com.example;

import org.junit.Test;

public class TASK2Test {

	@Test
	public void test() {
		
		TASK2<String> doublyLinkedList = new TASK2<String>();
		doublyLinkedList.add("First Element");
		doublyLinkedList.add("Second Element");
		doublyLinkedList.add("Third Element");
		doublyLinkedList.add("Four Element");
		doublyLinkedList.add("Five Element");
		doublyLinkedList.add("Six Element");
		
		doublyLinkedList.iterate();
				
		doublyLinkedList.remove("Third Element");
		
		doublyLinkedList.iterate();
	}

}
