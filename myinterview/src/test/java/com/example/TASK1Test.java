package com.example;

import static org.junit.Assert.*;

import org.junit.Test;

public class TASK1Test {

	@Test
	public void palindromeTest() {
		TASK1 task1 = new TASK1();
		assertTrue(task1.isPalidrome("racecar"));
	}
	
	@Test
	public void notPalindromeTest() {
		TASK1 task1 = new TASK1();
		assertFalse(task1.isPalidrome("technology"));
	}

}
