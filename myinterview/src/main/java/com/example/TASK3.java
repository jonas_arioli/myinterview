package com.example;

import java.util.List;

/**
 * Write a list and add an aleatory number of Strings. In the end, print out how
 * many distinct itens exists on the list.
 *
 */
public class TASK3 {
 
	public int countDistinctItems(List<String> list) {
		return (int) list.stream().distinct().count();
	}
}
