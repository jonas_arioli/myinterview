package com.example;

import java.util.NoSuchElementException;

/**
 * Task here is to write a list. Each element must know the element before and
 * after it. Print out your list and them remove the element in the middle of
 * the list. Print out again.
 *
 * 
 */
public class TASK2<E> {

	private Node head;
	private Node tail;
	private int size;

	/**
	 * this class keeps track of each element information
	 * 
	 * @author java2novice
	 *
	 */
	private class Node {
		E element;
		Node next;
		Node prev;

		public Node(E element, Node next, Node prev) {
			this.element = element;
			this.next = next;
			this.prev = prev;
		}
	}

	/**
	 * adds element at the end of the list
	 * 
	 * @param element
	 */
	public void add(E element) {

		Node tmp = new Node(element, null, tail);
		if (tail != null) {
			tail.next = tmp;
		}
		tail = tmp;
		if (head == null) {
			head = tmp;
		}
		size++;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * this method walks forward through the linked list
	 */
	public void iterate() {
		Node tmp = head;
		while (tmp != null) {
			System.out.println(tmp.element);
			tmp = tmp.next;
		}
	}

	/**
	 * this method removes element from the start of the linked list
	 * 
	 * @return
	 */
	private E removeFirst() {
		if (size == 0)
			throw new NoSuchElementException();
		Node tmp = head;
		head = head.next;
		head.prev = null;
		size--;
		return tmp.element;
	}

	/**
	 * this method removes element from the end of the linked list
	 * 
	 * @return
	 */
	private E removeLast() {
		if (size == 0)
			throw new NoSuchElementException();
		Node tmp = tail;
		tail = tail.prev;
		tail.next = null;
		size--;
		return tmp.element;
	}

	/**
	 * This method remove the element from list
	 * 
	 * @param element
	 */
	void remove(E element) {
		if (size == 0)
			throw new NoSuchElementException();
		if (head == tail) {
			head = null;
			tail = null;
		} else if (element == tail) {
			removeLast();
		} else if (element == head) {
			removeFirst();
		} else {
			Node current = head;
			while (current.element != element) {
				current = current.next;
			}
			current.prev.next = current.next;
			if (current.next != null) {
				current.next.prev = current.prev;
			}
			current = null;
		}

		size--;
	}
}
